const http = require('http');
const express = require('express')
const app = express()
const server = http.createServer(app);
const mongoose = require('mongoose');
const funcArgo = require('./func/argonaute')
const bodyParser = require('body-parser')

mongoose.set('useUnifiedTopology', true);
mongoose.connect('mongodb+srv://jeanne:jeannemdp@cluster0-tszkk.mongodb.net/wild?retryWrites=true&w=majority',  {useNewUrlParser: true}, )

.then(()=> console.log('ok'))
.catch(()=> console.log('no'))

const port = 8000;

server.on('listening',() => {
    console.log('test')
})

//Définition des CORS
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// method pour transformer le corps de la requete en json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/argonaute', funcArgo.getArgo);
  
app.post('/argonaute', funcArgo.postArgo);

server.listen(port);