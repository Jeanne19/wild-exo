import React from 'react';
import Members from './Members';

class Formulaire extends React.Component {
    constructor(props) {
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.updatestate = this.updatestate.bind(this)
        this.fetchArgo = this.fetchArgo.bind(this)
        this.state = { value: '',
        isSubmit : false,
        arrMembers : []
        }
    }

    updatestate(e) {
        let tval =  e.target.value.replace(/\d/g, '').replace(/^./, e.target.value.charAt(0).toUpperCase())
        this.setState({
            value : tval,
            isSubmit : false
        })
    }

    fetchArgo() {
        fetch("http://localhost:8000/argonaute", {
            method: "GET",
        })
        .then(res => {
            return res.json();
        })
        .then(json => {
            this.setState({arrMembers : json});
        });
    }

    componentDidMount() {
        this.fetchArgo()
    }

    handleSubmit(e) {
        e.preventDefault()
        fetch("http://localhost:8000/argonaute", {
            method: "POST",
            body: JSON.stringify({name : this.state.value}),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(res => {
            this.setState({ isSubmit: true });
            return res.json();
        })
        .then(data => {
            console.log(data.message);
        })
        .then(() => {
            this.fetchArgo()
        })
    }

    render() {
        const {value, isSubmit, arrMembers} = this.state
        const {title} = this.props
        return (
            <div id="argonautes" className="argonautes">
                <section className="argonautes__formulaire">
                    <h2 className='argonautes__formulaire__title'>{title}</h2>
                    <form action="/" method="POST" className="argonautes__formulaire__form" id="contact-form">
                        <label htmlFor="name">Nom de l&apos;Argonaute</label>
                        <input onChange={this.updatestate} id="name" name="name" type="text" placeholder="Charalampos" className="argonautes__formulaire__form__input" value={value}/>
                        <button onClick={this.handleSubmit} id='submit-button' type="submit" className="argonautes__formulaire__form__button">Envoyer</button>
                    </form>
                </section>
                {isSubmit &&  <div>{value} à été ajouté à la liste</div>}
                <Members clazzName={`argonautes`} members={arrMembers}></Members>
            </div>
        )
    }
}

export default Formulaire;
