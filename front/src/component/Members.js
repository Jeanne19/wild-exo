import React from 'react';

function Members({members, clazzName}){
    return (
        <section className={clazzName ? `${clazzName}__liste` : 'liste'}>
            <h2>Membres de l'équipage</h2>
           {members && <div className={clazzName ? `${clazzName}__liste__wrapper` : 'liste__wrapper'}>
                 {members.map((item, index) => {
                    return (<h3 key={index} className={clazzName ? `${clazzName}__liste__wrapper__name` : 'liste__wrapper__name'}> {item.name} </h3>)
                })}
            </div>}
        </section>
    )
}

export default Members;
