import React from 'react';

function Footer() {
    return (
        <footer className='footer' id="footer">
            <p>Réalisé par Jason en Anthestérion de l'an 515 avant JC</p>
        </footer>
    )
}

export default Footer
