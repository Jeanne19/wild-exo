import React from 'react';
import './App.css';
import Header from '../src/component/Header';
import Formulaire from '../src/component/Formulaire';
import Footer from '../src/component/Footer';


function App() {
  return (
    <div className="app">
          <Header name={'Les Argonautes'}></Header>
          <Formulaire title={'Ajouter un(e) Argonaute'}/>
          <Footer/>
    </div>
  );
}

export default App;
